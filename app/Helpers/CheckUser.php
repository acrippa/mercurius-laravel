<?php

namespace Mercurius\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

trait CheckUser
{

    public static function bootCheckUser()
    {
        static::addGlobalScope('user', function (Builder $builder) {
            $builder->where('user_id', Auth::id());
        });
    }

}
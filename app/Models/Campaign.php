<?php

namespace Mercurius\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Mercurius\Helpers\CheckUser;
use Mercurius\Helpers\UuidForKey;

class Campaign extends Model
{
    use CheckUser, SoftDeletes, UuidForKey;

    protected $keyType = 'string';

    function emails()
    {
        return $this->hasMany('Mercurius\Email');
    }

    function template()
    {
        return $this->belongsTo('Mercurius\Template');
    }

    function list()
    {
        return $this->belongsTo('Mercurius\List');
    }

    function send($send)
    {
        if(!$this->send && $send){
            $query = 'id, name, email FROM subscribers JOIN list_subscriber ON id = subscriber_id AND list_id = ?';
            DB::insert('INSERT INTO emails (id, campaign_id, status, created_at, subscriber_id, name, email) SELECT UUID(), ?, ?, NOW(), ' . $query, [$this->id, Email::QUEUED, $this->list_id]);
        }
    }

}

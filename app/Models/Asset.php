<?php

namespace Mercurius\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use League\Glide\ServerFactory;
use League\Glide\Responses\LaravelResponseFactory;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;
use Mercurius\Helpers\CheckUser;
use Mercurius\Helpers\UuidForKey;

class Asset extends Model
{
    use CheckUser, SoftDeletes, UuidForKey;

    protected $keyType = 'string';

    static function paths()
    {
        $assets = self::orderBy('created_at', 'DESC')->get();
        $paths = [];
        foreach($assets as $asset){
            $path = new \stdClass();
            $path->src = asset('storage/assets/'.$asset->id . '.' . $asset->extension);
            $paths[] = $path;
        }
        return $paths;
    }

    public function response(){
        $glideServer = ServerFactory::create([
            'response' => new LaravelResponseFactory(),
            'source' => Flysystem::connection(),
            'cache' => new Filesystem(new MemoryAdapter())
        ]);
        return $glideServer->getImageResponse($this->id,[]);
    }

    public function path()
    {
        return asset('storage/assets/'.$this->id . '.' . $this->extension);
    }

}

<?php

namespace Mercurius\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mercurius\Helpers\CheckUser;
use Mercurius\Helpers\UuidForKey;

class Subscriber extends Model
{
    use CheckUser, SoftDeletes, UuidForKey;

    protected $keyType = 'string';

    function emails()
    {
        return $this->hasMany('Mercurius\Email');
    }

    function lists()
    {
         return $this->belongsToMany('Mercurius\SubscribersList');
    }

}
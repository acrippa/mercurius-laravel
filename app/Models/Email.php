<?php

namespace Mercurius\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mercurius\Helpers\CheckUser;
use Mercurius\Helpers\UuidForKey;

class Email extends Model
{
    use CheckUser, SoftDeletes, UuidForKey;

    protected $keyType = 'string';

    const QUEUED = 100;
    const SENT = 200;
    const FAILED = 300;
    const READ = 400;

    public function subscriber()
    {
        return $this->belongsTo('Mercurius\Subscriber');
    }

    public function campaign()
    {
        return $this->belongsTo('Mercurius\Campaign');
    }

}
<?php

namespace Mercurius\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mercurius\Helpers\CheckUser;
use Mercurius\Helpers\UuidForKey;

class Template extends Model
{
    use CheckUser, SoftDeletes, UuidForKey;

    protected $keyType = 'string';

}

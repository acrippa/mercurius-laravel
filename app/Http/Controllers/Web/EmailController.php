<?php

namespace Mercurius\Http\Controllers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use League\Glide\ServerFactory;
use League\Glide\Responses\LaravelResponseFactory;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;
use Mercurius\Email;
use Mercurius\Campaign;

class EmailController extends Controller
{
    public function index(Email $email)
    {
        $email->status = Email::READ;
        $email->save();
        $html = $email->campaign->template->html;
        $html = str_replace("{{ app_url }}", env('APP_URL'), $html);
        $html = str_replace("{{ id }}", $email->id, $html);
        if($email->customization){
            foreach(json_decode($email->customization) as $key => $val)
            {
                $html = str_replace("{{ $key }}", $val, $html);
            }
        }
        return response()
        ->view('email', ['html' => $html]);
    }

    public function read(Email $email)
    {
        $email->status = Email::READ;
        $email->save();
        $glideServer = ServerFactory::create([
            'response' => new LaravelResponseFactory(),
            'source' => Flysystem::connection(),
            'cache' => new Filesystem(new MemoryAdapter())
            ]);
        return $glideServer->getImageResponse('private/1px.png',[]);
    }

    public function redirect(Email $email, $link)
    {
        $email->opened = true;
        $email->save();
        return redirect($link);
    }
}

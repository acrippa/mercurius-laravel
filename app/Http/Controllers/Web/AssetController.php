<?php

namespace Mercurius\Http\Controllers;

use Mercurius\Controller;
use Mercurius\Asset;

class AssetController extends Controller
{
    public function index(Asset $asset)
    {
        return $asset->response();
    }
}
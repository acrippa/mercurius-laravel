<?php

namespace Mercurius\Http\Controllers\Api\V1;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mercurius\Http\Controllers\Controller;
use Mercurius\Models\Campaign;

class CampaignController extends Controller
{

    public function campaigns()
    {
        try{
            return Campaign::all(['id', 'subject', 'list_id', 'sendtime', 'send']);
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function campaign(Campaign $campaign)
    {
        return $campaign;
    }

    public function store(Request $request, Campaign $campaign = null)
    {
        try{
            if (!$campaign){
                $campaign = Campaign::create($request->except('sent'));
            }else{
                $campaign->update($request->except('sent'));
            }
            $campaign->send($request->input('send'));
            return $campaign;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function destroy(Campaign $campaign)
    {
        try{
            $campaign->delete();
            return true;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function statistics(Request $request, Campaign $campaign)
    {
        try{
            $read = $campaign->newsletters()->where('status', Newsletter::READ)->get()->count();
            $failed = $campaign->newsletters()->where('status', Newsletter::FAILED)->get()->count();
            $delivered = $campaign->newsletters()->where('status', Newsletter::SENT)->get()->count();
            $total = $campaign->newsletters()->count();
            return [
                'read' => $read, 
                'failed' => $failed,
                'delivered' => $delivered,
                'total' => $total
            ];
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }
}

<?php

namespace Mercurius\Http\Controllers\Api\V1;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Mercurius\Http\Controllers\Controller;
use Mercurius\Models\Asset;

class AssetController extends Controller
{
    public function index(Request $request)
    {
        return Asset::paths();
    }

    public function upload(Request $request)
    {
        $files = $request->file('files');
        $paths = [];
        foreach($files as $file){
            $mimetype = mime_content_type($file->getRealPath());
            if(in_array($mimetype, ['image/jpeg','image/gif','image/png','image/bmp'])){
                $asset = new Asset;
                $asset->filename = $file->getClientOriginalName();
                $asset->extension = $file->getClientOriginalExtension();
                $asset->mimetype = $mimetype;
                $stream = fopen($file->getRealPath(), 'r+');
                if(is_resource($stream) && $asset->save() && Flysystem::writeStream( 'public/assets/' . $asset->id . '.' . $asset->extension, $stream)){
                    fclose($stream);
                }
                $paths[] = $asset->path();
            }
        }
        return ['data' => $paths];
    }
}
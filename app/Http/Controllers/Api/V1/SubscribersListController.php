<?php

namespace Mercurius\Http\Controllers\Api\V1;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mercurius\Http\Controllers\Controller;
use Mercurius\Models\SubscribersList;

class SubscribersListController extends Controller
{

    public function lists()
    {
        try{
            return SubscribersList::all(['id', 'name']);
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function list(SubscribersList $list)
    {
        return $list;
    }

    public function store(Request $request, SubscribersList $list = null)
    {
        try{
            if (!$list){
                $list = SubscribersList::create($request->except('sent'));
            }else{
                $list->update($request->except('sent'));
            }
            $list->send($request->input('send'));
            return $list;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function destroy(SubscribersList $list)
    {
        try{
            $list->delete();
            return true;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

}

<?php

namespace Mercurius\Http\Controllers\Api\V1;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mercurius\Http\Controllers\Controller;
use Mercurius\Models\Template;

class TemplateController extends Controller
{

    public function templates()
    {
        try{
            return Template::all('id', 'title', 'description', 'html');
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function template(Template $template)
    {
        return $template;
    }

    public function preview(Template $template)
    {
        return $template->html;
    }

    public function thumbnail(Template $template)
    {
        
    }

    public function store(Request $request, Template $template = null)
    {
        try{
            $data = $request->all();
            $data['mjml'] = preg_replace('/(<mj-image[^<>]*)(\/>)/i', '$1></mj-image>', $data['mjml']);
            if ($template){
                $template->update($data);
            }else{
                $template = Template::create($data);
            }
            return $template;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function delete(Template $template)
    {
        try{
            $template->delete();
            return true;
        }catch(\Exception $ex){
            return response()->json( ['message'=>$ex->getMessage()], 500 );
        }
    }

    public function test(Request $request, $template)
    {

    }
}

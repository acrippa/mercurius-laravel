<?php

Route::group(['prefix' => 'email','namespace' => 'Web'], function() {
    Route::get('/{id}', 'EmailController@index');
    Route::get('/{id}/read', 'EmailController@read');
    Route::get('/{id}/link/{link}', 'EmailController@link');
});

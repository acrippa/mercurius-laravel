<?php

Route::group(['prefix' => 'v1/campaigns','namespace' => 'Api\V1', 'middleware' => ['auth:api']], function() {
    Route::get('/', [ 
        'as' => 'campaign.campaigns', 'uses' => 'CampaignController@campaigns'
    ]);
    Route::get('/{campaign}', [
        'as' => 'campaign.get', 'uses' => 'CampaignController@campaign'
    ]);

    Route::post('/', [
        'as' => 'campaign.create', 'uses' => 'CampaignController@store'
    ]);
    Route::put('/{campaign}', [
        'as' => 'campaign.update', 'uses' => 'CampaignController@store'
    ]);
    Route::delete('/{campaign}', [
        'as' => 'campaign.delete', 'uses' => 'CampaignController@delete'
    ]);
});
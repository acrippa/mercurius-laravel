<?php

Route::group(['prefix' => 'v1/templates','namespace' => 'Api\V1', 'middleware' => ['auth:api']], function() {
    Route::get('/', [ 
        'as' => 'template.templates', 'uses' => 'TemplateController@templates'
    ]);
    Route::get('/{template}', [
        'as' => 'template.get', 'uses' => 'TemplateController@template'
    ]);
    Route::get('/{template}/preview', [
        'as' => 'template.preview', 'uses' => 'TemplateController@preview'
    ]);
    Route::get('/{template}/thumbnail', [
        'as' => 'template.thumbnail', 'uses' => 'TemplateController@thumbnail'
    ]);

    Route::post('/', [
        'as' => 'template.create', 'uses' => 'TemplateController@store'
    ]);
    Route::put('/{template}', [
        'as' => 'template.update', 'uses' => 'TemplateController@store'
    ]);
    Route::delete('/{template}', [
        'as' => 'template.delete', 'uses' => 'TemplateController@delete'
    ]);
});
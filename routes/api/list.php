<?php

Route::group(['prefix' => 'v1/lists','namespace' => 'Api\V1', 'middleware' => ['auth:api']], function() {
    Route::get('/', [ 
        'as' => 'list.lists', 'uses' => 'SubscribersListController@lists'
    ]);
    Route::get('/{list}', [
        'as' => 'list.get', 'uses' => 'SubscribersListController@list'
    ]);

    Route::post('/', [
        'as' => 'list.create', 'uses' => 'SubscribersListController@store'
    ]);
    Route::put('/{list}', [
        'as' => 'list.update', 'uses' => 'SubscribersListController@store'
    ]);
    Route::delete('/{list}', [
        'as' => 'list.delete', 'uses' => 'SubscribersListController@delete'
    ]);
});
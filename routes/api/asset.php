<?php

Route::group(['prefix' => 'v1/assets','namespace' => 'Api\V1', 'middleware' => ['auth:api']], function() {
    Route::get('/', [ 
        'as' => 'asset.assets', 'uses' => 'AssetController@index'
    ]);
    Route::post('/', [
        'as' => 'asset.upload', 'uses' => 'AssetController@upload'
    ]);
});

    
    
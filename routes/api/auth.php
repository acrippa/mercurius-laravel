<?php

Route::group(['prefix' => 'v1/auth', 'namespace' => 'Api\V1'], function () {
    Route::post('', 'AuthController@login');
    Route::group(['middleware' => ['auth:api']], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
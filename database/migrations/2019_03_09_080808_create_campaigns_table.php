<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->unsignedBigInteger('user_id');
            $table->uuid('template_id')->nullable();
            $table->uuid('list_id')->nullable();
            $table->string('subject');
            $table->integer('send')->nullable();
            $table->datetime('sendtime')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->softDeletes();  
            $table->index('subject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
